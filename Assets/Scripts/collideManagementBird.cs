﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class collideManagementBird : MonoBehaviour
{

    private GameObject bird;

    void Start()
    {
        bird = GameObject.Find("bird1");
    }

    void Update()
    {
        
    }

    void OnTriggerEnter2D(Collider2D other){
        if(other.tag=="pipe"){
            bird.GetComponent<endAction>().stop();
        }
        if(other.tag=="scoreUp"){
            GameObject singleton = GameObject.FindWithTag("singleton");
            singleton.GetComponent<singleton>().addScore();
        }
    }
}
