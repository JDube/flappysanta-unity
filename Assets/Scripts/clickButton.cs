﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class clickButton : MonoBehaviour
{
    public String nameScene;
    
    public void onClick(){
	    SceneManager.LoadScene(nameScene);
    }
}
