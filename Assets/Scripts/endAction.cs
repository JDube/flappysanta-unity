﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class endAction : MonoBehaviour
{
    private GameObject bird;

    // Start is called before the first frame update
    void Start()
    {
        bird = GameObject.Find("bird1");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void stop()
    {
        Destroy(bird.GetComponent<touchAction>());
        Destroy(bird.GetComponent<collideManagementBird>());
        Destroy(bird.GetComponent<posSanta>());
        foreach (GameObject background in GameObject.FindGameObjectsWithTag("background")){
            background.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
            Destroy(background.GetComponent<moveBk>());
        }
        foreach (GameObject pipe in GameObject.FindGameObjectsWithTag("pipe")){
            pipe.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
        }
        foreach (GameObject pipePair in GameObject.FindGameObjectsWithTag("pipePair")){
            Destroy(pipePair.GetComponent<movePipes>());
        }
        StartCoroutine(TestCoroutine());
    }

    IEnumerator TestCoroutine(){
         foreach (GameObject brick in GameObject.FindGameObjectsWithTag("brick")){
             brick.transform.position = new Vector3(brick.transform.position.x,brick.transform.position.y-10,brick.transform.position.z);
             float time = Convert.ToSingle(0.05);
             yield return new WaitForSeconds(time);
         }
	    SceneManager.LoadScene("scene4-GameOver");
       }
}
