﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;

public class wait2sec : MonoBehaviour {

	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {
        StartCoroutine(LoadCoroutine());
		Invoke("goScene2",2.0f);
	}

	public void goScene2(){
        SceneManager.LoadScene("scene2-Menu");
    }

    IEnumerator LoadCoroutine(){
         GameObject.FindWithTag("brick").GetComponent<SpriteRenderer> ().color = new Color(1f,1f,1f,1f);
         float time = Convert.ToSingle(0.35);
         yield return new WaitForSeconds(time);
         GameObject.FindWithTag("brick1").GetComponent<SpriteRenderer> ().color = new Color(1f,1f,1f,1f);
         yield return new WaitForSeconds(time);
         GameObject.FindWithTag("brick2").GetComponent<SpriteRenderer> ().color = new Color(1f,1f,1f,1f);
         yield return new WaitForSeconds(time);
         GameObject.FindWithTag("brick3").GetComponent<SpriteRenderer> ().color = new Color(1f,1f,1f,1f);
         yield return new WaitForSeconds(time);
         GameObject.FindWithTag("brick4").GetComponent<SpriteRenderer> ().color = new Color(1f,1f,1f,1f);
         yield return new WaitForSeconds(time);
         GameObject.FindWithTag("brick5").GetComponent<SpriteRenderer> ().color = new Color(1f,1f,1f,1f);
         yield return new WaitForSeconds(time);

    }
}