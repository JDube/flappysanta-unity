﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class openGift : MonoBehaviour
{
    public double maxHeight;
    public double wait;
	private Vector3 movement;

    // Start is called before the first frame update
    void Start()
    {
        GameObject singleton = GameObject.FindWithTag("singleton");
        singleton.GetComponent<singleton>().getScoreGameOver();
	    StartCoroutine(TestCoroutine());
    }

    // Update is called once per frame
    void Update()
    {
		if(transform.position.y<maxHeight){
		    GetComponent<Rigidbody2D>().velocity = movement;
		}
		else{
            GetComponent<Rigidbody2D>().velocity = new Vector3( 0, 0, 0);
        }
    }

    IEnumerator TestCoroutine(){
        float time = Convert.ToSingle(wait);
        yield return new WaitForSeconds(time);
        movement = new Vector3( 0, 5, 0);
   }
}
