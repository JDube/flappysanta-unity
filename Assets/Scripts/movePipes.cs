﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movePipes : MonoBehaviour {

    public Vector2 movement;
    private Vector2 siz;
    private Vector2 position;
    public GameObject pipe1Up, pipe1Down, scorecollide;
    private Transform pipe1UpOriginalTransform, pipe1DownOriginalTransform, scorecollideOriginalTransform;
    private Vector2 leftBottomCameraBorder, rightBottomCameraBorder;

    void Start () {
        leftBottomCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(0,0,0));
        rightBottomCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3(1,0,0));
        pipe1UpOriginalTransform = pipe1Up.GetComponent<Rigidbody2D>().transform;
        pipe1DownOriginalTransform = pipe1Down.GetComponent<Rigidbody2D>().transform;
        scorecollideOriginalTransform = scorecollide.GetComponent<Rigidbody2D>().transform;
    }

    // Update is called once per frame
    void Update () {
        pipe1Up.GetComponent<Rigidbody2D>().velocity = movement;
        pipe1Down.GetComponent<Rigidbody2D>().velocity = movement;
        scorecollide.GetComponent<Rigidbody2D>().velocity = movement;
        siz.x = pipe1Up.GetComponent<SpriteRenderer> ().bounds.size.x;
        siz.y = pipe1Up.GetComponent<SpriteRenderer> ().bounds.size.y;
        if (pipe1Up.transform.position.x < leftBottomCameraBorder.x - (siz.x / 2)) moveToRightPipe();

    }

    void moveToRightPipe(){
        float randomY = Random.Range (1,4) - 2;
        float posX = rightBottomCameraBorder.x*2 + (siz.x / 2);
        float posY = pipe1UpOriginalTransform.position.y + randomY;
        Vector3 tmpPos = new Vector3 (posX, posY, pipe1Up.transform.position.z);
        pipe1Up.transform.position = tmpPos;

        posY = pipe1DownOriginalTransform.position.y + randomY;
        tmpPos = new Vector3 (posX,posY, pipe1Down.transform.position.z);
        pipe1Down.transform.position = tmpPos;

        tmpPos = new Vector3 (posX,scorecollide.transform.position.y, scorecollide.transform.position.z);
        scorecollide.transform.position = tmpPos;
    }
}

