﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moveBk : MonoBehaviour
{
	
	private Vector3 movement;
	private Vector3 size;
	private Vector3 coinBasGauche;
	public float positionRestartX;
	
    // Start is called before the first frame update
    void Start()
    {
	    movement = new Vector3(-1, 0, 0);
        coinBasGauche = Camera.main.ViewportToWorldPoint (new Vector3(0, 0, 0));
    }

    // Update is called once per frame    
    void Update(){
	    
		GetComponent<Rigidbody2D>().velocity = movement;
		size.x = gameObject.GetComponent<SpriteRenderer> ().bounds.size.x; 
		size.y = gameObject.GetComponent<SpriteRenderer> ().bounds.size.y;

		if (transform.position.x < coinBasGauche.x - (size.x / 2)) {
			transform.position = new Vector3(positionRestartX,transform.position.y,transform.position.z); 
		}
	}
}
