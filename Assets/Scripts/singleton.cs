﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class singleton : MonoBehaviour
{
 private static singleton instance = null;
 public int score;

 // Game Instance Singleton
 public static singleton Instance
 {
     get
     {
         return instance;
     }
 }

 private void Awake()
 {
     if (instance != null && instance != this)
     {
         Destroy(this.gameObject);
     }

     instance = this;
     DontDestroyOnLoad( this.gameObject );
 }

 void Update()
 {
 }

 public void addScore()
  {
      score += 1;
      Text text = GameObject.FindWithTag("score").GetComponent<Text>();
      text.text = (score/2).ToString();
  }

  public void getScoreGameOver(){
      Text textGameOver = GameObject.FindWithTag("scoreGameOver").GetComponent<Text>();
      textGameOver.text = String.Concat("Score: ", (score/2).ToString());
  }
}